package com.muraai.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import org.springframework.util.StringUtils;



public class AutomatedClassGeneration {
	
    
	public static void main(String[] args) {
		if(args==null)
		{
			System.out.println("Please enter atleast 1 argument as Table Name");
			return;
		}
		Connection con;
		FileWriter fw=null;
		BufferedWriter bw = null;
		try {
			Class.forName(Constants.DB_DRIVER);
			con=DriverManager.getConnection(Constants.DB_URL,Constants.DB_USERNAME,Constants.DB_PASSWORD);  
			Statement stmt=con.createStatement();  
			if(args !=null){
				for(String str : args){
					
					ResultSet rs=stmt.executeQuery(Constants.SELECT_QUERY +" "+ str);
					ResultSetMetaData rsmd = rs.getMetaData();
					int columnCount = rsmd.getColumnCount();
					System.out.println(columnCount);
					String camelCaselName = getExactCamelCaseName(str);
					
					File file = new File(Constants.FILE_PATH+ camelCaselName + Constants.FILE_EXTENSION);
					System.out.println(camelCaselName + file);
					fw = new FileWriter(file);
					bw = new BufferedWriter(fw);
					bw.write(Constants.COLUMN_ANNOTATION_IMPORT+"\n");
					bw.write("public class " + camelCaselName+" { \n\n");
					for (int i = 1; i <= columnCount; i++ ) {
					  String columnName = rsmd.getColumnName(i);
					  String columnType = rsmd.getColumnTypeName(i);
					  System.out.println("Column Name and column type are " + columnName+"  "+columnType);
					  String content = prepareContent(columnName,columnType);
					  bw.write("@Column(name=\""+""+columnName+"\")\n");
					  bw.write(content);
					  bw.write("\n");
					}
					bw.write("\n }");
					System.out.println("File " + str + " created and written");
					bw.flush();
					
				}
			}
			con.close();
			if(fw !=null)
				fw.close();
			if(bw !=null)
				bw.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private static String getExactCamelCaseName(String str) {
		String camelCaseString="";
		if(StringUtils.hasText(str)){
			
			if(str.contains("_")){
				String[] array = str.split("_");
				if(array !=null){
					for(String s :array){
						
						camelCaseString = camelCaseString+convertToCamelCase(s);
					}
				}
			}
			else{
				camelCaseString = camelCaseString + convertToCamelCase(str);
			}
			return camelCaseString;
		}
		else
			return "";
	}
	private static String getExactCamelCaseNameForProperty(String str) {
		String camelCaseString="";
		if(StringUtils.hasText(str)){
			
			if(str.contains("_")){
				String[] array = str.split("_");
				if(array !=null){
					for(String s :array){
						if(array[0]==s)
						camelCaseString = camelCaseString+convertToCamelCaseForProperty(s);
						else
							camelCaseString = camelCaseString+convertToCamelCase(s);	
					}
				}
			}
			else{
				camelCaseString = camelCaseString + convertToCamelCaseForProperty(str);
			}
			return camelCaseString;
		}
		else
			return "";
	}

	private static String prepareContent(String columnName, String columnType) {
		StringBuilder content = new StringBuilder();
		String javaType = getJavaTypeFromColumnType(columnType);
		String javaProperty = getExactCamelCaseNameForProperty(columnName);
		
		if(javaType !=null && javaProperty !=null)
			content.append("private"+" " + javaType + " " + javaProperty + ";\n");
		
		return content.toString();
	}

	private static String getJavaTypeFromColumnType(String columnType) {
		switch (columnType) {
		case "INT":
			return "Integer";
		
		case "SMALLINT":
			return "Short";
		
		case "BIGINT":
			return "BigInteger";
		
		case "DOUBLE":
			return "Double";
		
		case "VARCHAR":
			return "String";
		case "TEXT":
			return "String";
			
		case "BLOB":
			return "byte[]";

		case "DATE":
			return "Date";
		case "DATETIME":
			return "Date";
		
		
		case "TIMESTAMP":
			return "Date";
		case "DECIMAL":
			return "BigDecimal";
		
		}
		return "";
	}

	private static String convertToCamelCase(String str) {
		String result = "";
		char firstChar = str.charAt(0);
        result = result + Character.toUpperCase(firstChar);
        for (int i = 1; i < str.length(); i++) {
            char currentChar = str.charAt(i);
            char previousChar = str.charAt(i - 1);
            if (previousChar == ' ') {
                result = result + Character.toUpperCase(currentChar);
            } else {
                result = result + currentChar;
            }
        }
        System.out.println(result);
        return result;
	}
	
	private static String convertToCamelCaseForProperty(String str) {
		String result = "";
		char firstChar = str.charAt(0);
        result = result + Character.toLowerCase(firstChar);
        for (int i = 1; i < str.length(); i++) {
            char currentChar = str.charAt(i);
            char previousChar = str.charAt(i - 1);
            if (previousChar == ' ') {
                result = result + Character.toUpperCase(currentChar);
            } else {
                result = result + currentChar;
            }
        }
        System.out.println(result);
        return result;
	}
}
