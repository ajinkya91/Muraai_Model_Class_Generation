package com.muraai.main;

public class Constants {

	public static final String DB_DRIVER ="com.mysql.jdbc.Driver";
	public static final String DB_URL ="jdbc:mysql://192.168.0.148:3306/muraai_ap";
	public static final String DB_USERNAME ="root";
	public static final String DB_PASSWORD ="muraai";
	public static final String SELECT_QUERY ="select * from ";
	public static final String FILE_PATH ="D:\\Ajinkya\\Automated Java Class\\";
	public static final String FILE_EXTENSION =".java";
	public static final String COLUMN_ANNOTATION_IMPORT="import javax.persistence.Column;";
	
}
