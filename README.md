This project will generate Model Class for the table name given as input.

It is a standalone application which accepts table name as run parameter and will write java model class to a particular location . 

File location is customizable which needs to be changed in constant file. Bafore running you need to change file location where the file is to be written.

Database properties are customizable which can be altered from constant file provided in the project.